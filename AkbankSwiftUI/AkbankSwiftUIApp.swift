//
//  AkbankSwiftUIApp.swift
//  AkbankSwiftUI
//
//  Created by Onur on 14.12.2022.
//

import SwiftUI

@main
struct AkbankSwiftUIApp: App {
    @State var isUser : Bool = false
    @State var isLaunchScreen : Bool = true
    var body: some Scene {
        WindowGroup {
            NavigationView {
                if isLaunchScreen {
                    LaunchView()
                        .onAppear {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                                withAnimation(.easeInOut(duration: 0.8)) {
                                self.isLaunchScreen = false
                              }
                            }
                        }
                } else {
                    if isUser {
                        MainView()
                    } else {
                        LoginView(isUser: $isUser)
                    }
                }
                                
            }
        }
    }
}
