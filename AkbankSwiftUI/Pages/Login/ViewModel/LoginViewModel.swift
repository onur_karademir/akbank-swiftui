//
//  LoginViewModel.swift
//  AkbankSwiftUI
//
//  Created by Onur on 15.12.2022.
//

import Foundation


struct LoginModel : Identifiable {
    var id = UUID()
    var icon : String
    var title : String
}


class LoginViewModel : ObservableObject {
    
    @Published var loginArray : [LoginModel] = []
    
    init(){
        getItem()
    }
    
    func getItem() {
        let item1 = LoginModel(icon: "printer.fill", title: "ATM'den para cek yatir")
        let item2 = LoginModel(icon: "qrcode", title: "QR ile ode")
        let item3 = LoginModel(icon: "lock.open.iphone", title: "Doviz kurlari")
        let item4 = LoginModel(icon: "house.fill", title: "En yakin Akbank")
        
        loginArray.append(contentsOf: [item1, item2, item3, item4])
    }
}
