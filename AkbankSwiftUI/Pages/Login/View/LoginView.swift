//
//  LoginView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 15.12.2022.
//

import SwiftUI

struct LoginView: View {
    @StateObject var viewmodel : LoginViewModel = LoginViewModel()
    @Binding var isUser : Bool
    @State var pass = ""
    var body: some View {
        VStack {
            ScrollView(.vertical, showsIndicators: false) {
                
                VStack (spacing: 13) {
                    
                    LoginTabView()
                    
                    LoginUserIconView(
                        loginUserName: "OK",
                        loginUserFont: .title,
                        loginUserFontWeight: .bold,
                        loginUserNameColor: Color("ColorOne"),
                        loginCircleColor: Color("ColorOne"),
                        loginCircleWidth: 35,
                        loginCircleHeight: 35,
                        loginCircleImage: "person",
                        mainCircleColor: Color(.systemGray5),
                        mainCircleWidth: 90,
                        mainCircleHeight: 90
                    )
                    
                    LoginTitleSetView(
                        title: "İyi Günler",
                        subTitle: "ONUR",
                        fontSize: 45,
                        question: "Yoksa Onur degil misin?",
                        questionColor: Color("ColorOne")
                    )
                    SecureField("Parola", text: self.$pass)
                        .font(.title3)
                        .padding(10)
                        .autocapitalization(.none)
                        .foregroundColor(Color("ColorOne"))
                        .background(Color("ColorOne").opacity(0.2))
                        .cornerRadius(10)
                    Button {
                        // login action //
                        // fake user login //
                        // normally it will be done with firebase. //
                        isUser = true
                    } label: {
                        LoginButtonView(
                            loginText: "Giris",
                            loginButtonWidth: 150,
                            loginButtonColor: .white,
                            loginButtonPaddingVertical: 15,
                            loginButtonBgColor: Color("ColorOne")
                        )
                    }
                    
                    AppVersionView(
                        appVersion: "V 4.21.1",
                        appVersionFont: .caption,
                        appVersionFontWeight: .bold,
                        appVersionColor: .gray
                    )
                    
                    
                    
                }
                .padding()
            }
            VStack (spacing: 2) {
                HStack (spacing: 1) {
                    ForEach(viewmodel.loginArray) { item in
                        LoginIconRowView(
                            loginIconCicleColor: .white,
                            loginIconCicleWidth: 60,
                            loginIconCicleHeight: 60,
                            loginIcon: item.icon,
                            loginIconColor: Color("ColorOne"),
                            loginIconSize: 25,
                            loginIconText: item.title,
                            loginIconTextFont: .caption,
                            loginIconTextFontWeight: .semibold,
                            loginIconTextColor: .white,
                            loginIconTextWidth: 89,
                            loginIconTextHeight: 60
                        )
                    }
                }
                CurrencyView(
                    currency: "1 USD = 18.63 TL",
                    currencyFont: .caption2,
                    currencyFontWeight: .semibold,
                    currencyColor: .white
                )
            }
            .frame(
                  minWidth: 0,
                  maxWidth: .infinity,
                  minHeight: 0,
                  maxHeight: 160
                )
            .padding()
            .background(Color("ColorOne"))
        }
        .navigationBarTitle("")
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView(isUser: .constant(false))
    }
}
