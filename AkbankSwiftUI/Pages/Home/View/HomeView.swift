//
//  HomeView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 14.12.2022.
//

import SwiftUI

struct HomeView: View {
    @StateObject var viewModel : HomeCardsViewModel = HomeCardsViewModel()
    var body: some View {
        VStack {
            VStack (alignment: .leading, spacing: 0) {
                HomeHeaderView(
                    title: "AKBANK",
                    titleFont: .title2,
                    titleFontWeight: .bold,
                    titleColor: .white,
                    userName: "OK",
                    userNameColor: .red,
                    userNameFont: .headline
                )
                ScrollView(.vertical, showsIndicators: false) {
                    VStack (alignment: .leading, spacing: 30) {
                        HomeTitleSetView(
                            homeTitle: "HESAPLAR VE KARTLAR",
                            homeTitleFont: .footnote,
                            homeTitleFontWeight: .bold,
                            homeTitleColor: .gray,
                            homeCardsTitle: "TÜMÜ",
                            homeCardsFont: .caption,
                            homeCardsFontWeight: .bold,
                            homeCardsColor: .red
                        )
                        HomeMainCardView(
                            mainCardTitle: "ORNEK MAHALLESI",
                            mainCardTitleFont: .subheadline,
                            mainCardTitleFontWeight: .bold,
                            mainCardNum: "1234 - 005678",
                            mainCardNumFont: .footnote,
                            mainCardNumFontWeight: .bold,
                            mainCardNumColor: .gray,
                            mainCardSubTitle: "Kullanilabilir bakiye",
                            mainCardSubTitleFont: .headline,
                            mainCardSubTitleFontWeight: .bold,
                            mainCardSubTitleColor: .gray,
                            mainCardMoney: "10.000,50 TL",
                            mainCardMoneyFont: .title,
                            mainCardMoneyFontWeight: .bold,
                            accountType: "VADESIZ",
                            accountTypeFont: .subheadline,
                            accountTypeFontWeight: .semibold,
                            accountTypeColor: .black
                        )
                        HomeDividerTitleView(
                            productTitle: "URUNLER",
                            productTitleFont: .headline,
                            productTitleFontWeight: .bold,
                            productTitleColor: .gray
                        )
                        
                        ForEach(viewModel.homeData) { item in
                            HomeCardsView(
                                homeCardsTitle: item.title,
                                homeCardsTitleFont: .subheadline,
                                homeCardsTitleColor: .gray,
                                homeCardsTitleFontWight: .bold,
                                homeCardsSubTitle: item.subTitle,
                                homeCardsSubTitleFont: .callout,
                                homeCardsSubTitleColor: .black,
                                homeCardsSubTitleFontWight: .semibold,
                                homeCardsFooter: item.footerTitle,
                                homeCardsFooterFont: .subheadline,
                                homeCardsFooterFontWeight: .semibold,
                                homeCardsFooterColor: Color("ColorOne"),
                                homeCardsImage: item.icon,
                                reverse: item.reverse
                            )
                        }
                        HomeDividerTitleView(
                            productTitle: "KISAYOLLAR",
                            productTitleFont: .headline,
                            productTitleFontWeight: .bold,
                            productTitleColor: .gray
                        )
                        
                        ForEach(viewModel.shortcuts) { item in
                            ShortcutsView(
                                shortcutsCircleColor: Color(.systemGray5),
                                shortcutsIcon: item.icon,
                                shortcutsIconColor: Color("ColorOne"),
                                shortcutsTitle: item.title,
                                shortcutsTitleFont: .headline,
                                shortcutsTitleFontWeight: .semibold
                            )
                        }
                        
                        VStack (alignment: .leading, spacing: 5) {
                            ForEach(viewModel.footer) { item in
                                FooterView(
                                    footerTitle: item.title,
                                    footerSubTitle: item.subTitle,
                                    footerTitleFont: .caption2,
                                    footerTitleFontWeight: .bold,
                                    footerTitleColor: .gray,
                                    footerSubTitleFont: .caption2,
                                    footerSubTitleFontWeight: .bold,
                                    footerSubTitleColor: .gray
                                )
                            }
                        }
                        .padding(10)
                        .background(Color(.systemGray6))
                        .cornerRadius(5)
                    }
                    .padding()
                }
            }
        }
        .navigationBarTitle("")
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
