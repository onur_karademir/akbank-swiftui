//
//  LaunchView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 14.12.2022.
//

import SwiftUI

struct LaunchView: View {
    var body: some View {
        VStack {
            LaunchLogoView(
                launchLogoTitle: "AKBANK",
                launchLogoSize: 60,
                launchLogoWeight: .bold,
                launchLogoColor: .white
            )
        }
        .frame(
              minWidth: 0,
              maxWidth: .infinity,
              minHeight: 0,
              maxHeight: .infinity,
              alignment: .center
            )
        .background(Color("ColorOne"))
        .navigationBarTitle("")
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
}

struct LaunchView_Previews: PreviewProvider {
    static var previews: some View {
        LaunchView()
    }
}
