//
//  MainView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 15.12.2022.
//

import SwiftUI

struct MainView: View {
    @State var selectdTab : String = "house"
    var body: some View {
        // old version //
//        ZStack(alignment: .bottom) {
//            if selectdTab == "house" {
//                HomeView()
//            } else if selectdTab == "arrow.left.arrow.right.circle" {
//                PaymentsView()
//            } else {
//                DiscoveryView()
//            }
//            CustomTabView(selectdTab: $selectdTab)
//                .offset(y: 22)
//        }
        VStack(spacing: 0) {
            VStack {
                if selectdTab == "house" {
                    HomeView()
                } else if selectdTab == "arrow.left.arrow.right.circle" {
                    PaymentsView()
                } else {
                    DiscoveryView()
                }
            }
            VStack {
                CustomTabView(selectdTab: $selectdTab)
                    .offset(y: 22)
            }
        }
    }

}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
