//
//  DiscoveryView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 17.12.2022.
//

import SwiftUI

struct DiscoveryView: View {
    
    @StateObject var viewModel : DiscoveryViewModel = DiscoveryViewModel()
    
    @State var isOpenMenu : Bool = false
    @State var searchText = ""
    
    var body: some View {
        VStack (alignment: .leading) {
            
            RegularHeaderView(
                title: "Kesfet",
                titleColor: .white,
                titleFont: .title3,
                titleFontWeight: .bold,
                titleKerning: 1,
                headerBgColor: Color("ColorOne"),
                image: "bubble.left.and.bubble.right.fill",
                imageColor: .white,
                imageFont: .title,
                imageOffset: -10
            )
            
            VStack (alignment: .leading) {
                
                ScrollView(.vertical, showsIndicators: false) {
                    
                    VStack (alignment: .leading) {
                        
                        HStack {
                            Spacer()
                            DiscoverySearchView(
                                placeholder: "Ara",
                                radius: 15,
                                bindingText: $searchText
                            )
                            Spacer()
                        }
                        
                        HomeDividerTitleView(
                            productTitle: "SANA OZEL ONERILENLER",
                            productTitleFont: .subheadline,
                            productTitleFontWeight: .bold,
                            productTitleColor: .gray
                        )
                        
                        VStack (alignment: .center) {
                            
                            HStack (spacing: 13) {
                                
                                Spacer()
                                
                                ForEach(viewModel.discoveryArray) { item in
                                    LoginIconRowView(
                                        loginIconCicleColor: .gray.opacity(0.35),
                                        loginIconCicleWidth: 65,
                                        loginIconCicleHeight: 65,
                                        loginIcon: item.icon,
                                        loginIconColor: Color("ColorOne"),
                                        loginIconSize: 25,
                                        loginIconText: item.title,
                                        loginIconTextFont: .caption,
                                        loginIconTextFontWeight: .semibold,
                                        loginIconTextColor: .black.opacity(0.8),
                                        loginIconTextWidth: 80,
                                        loginIconTextHeight: 60
                                    )
                                }
                                Spacer()
                            }
                            
                            HStack (spacing: 13) {
                                Spacer()
                                ForEach(viewModel.discoveryArray.reversed()) { item in
                                    LoginIconRowView(
                                        loginIconCicleColor: .gray.opacity(0.35),
                                        loginIconCicleWidth: 65,
                                        loginIconCicleHeight: 65,
                                        loginIcon: item.icon,
                                        loginIconColor: Color("ColorOne"),
                                        loginIconSize: 25,
                                        loginIconText: item.title,
                                        loginIconTextFont: .caption,
                                        loginIconTextFontWeight: .semibold,
                                        loginIconTextColor: .black.opacity(0.8),
                                        loginIconTextWidth: 80,
                                        loginIconTextHeight: 60
                                    )
                                }
                                Spacer()
                            }
                            
                        }
                        .frame(
                              minWidth: 0,
                              maxWidth: .infinity,
                              minHeight: isOpenMenu ? 260 : 132,
                              maxHeight: isOpenMenu ? 260 : 132,
                              alignment: .topLeading
                            )
                        .padding()
                        .background(Color(.systemGray5))
                        .cornerRadius(10)
                        .overlay (
                            Button {
                                withAnimation {
                                    isOpenMenu.toggle()
                                }
                            } label: {
                                Image(systemName: isOpenMenu ? "chevron.up" : "chevron.down")
                                    .font(.system(size: 36, weight: .heavy))
                                    .foregroundColor(.red)
                            }
                            ,alignment: .bottom
                        )
                        
                        HomeDividerTitleView(
                            productTitle: "KISAYOLLAR",
                            productTitleFont: .subheadline,
                            productTitleFontWeight: .bold,
                            productTitleColor: .gray
                        )
                        .padding(.vertical)
                        
                        ForEach(viewModel.shortcuts.prefix(4)) { item in
                            ShortcutsView(
                                shortcutsCircleColor: Color(.systemGray5),
                                shortcutsIcon: item.icon,
                                shortcutsIconColor: Color("ColorOne"),
                                shortcutsTitle: item.title,
                                shortcutsTitleFont: .headline,
                                shortcutsTitleFontWeight: .semibold
                            )
                        }
                        
                        HomeDividerTitleView(
                            productTitle: "BIZE ULAS",
                            productTitleFont: .subheadline,
                            productTitleFontWeight: .bold,
                            productTitleColor: .gray
                        )
                        .padding(.vertical)
                        
                        VStack (alignment: .center) {
                            
                            HStack (spacing: 30) {
                                
                                Spacer()
                                
                                ForEach(viewModel.contacts) { item in
                                    LoginIconRowView(
                                        loginIconCicleColor: .gray.opacity(0.2),
                                        loginIconCicleWidth: 85,
                                        loginIconCicleHeight: 85,
                                        loginIcon: item.icon,
                                        loginIconColor: Color("ColorOne"),
                                        loginIconSize: 30,
                                        loginIconText: item.title,
                                        loginIconTextFont: .headline,
                                        loginIconTextFontWeight: .semibold,
                                        loginIconTextColor: .black.opacity(0.8),
                                        loginIconTextWidth: 89,
                                        loginIconTextHeight: 60
                                    )
                                }
                                
                                Spacer()
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            .padding()
        }
        .navigationBarTitle("")
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
}

struct DiscoveryView_Previews: PreviewProvider {
    static var previews: some View {
        DiscoveryView()
    }
}
