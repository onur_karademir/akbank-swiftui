//
//  DiscoveryViewModel.swift
//  AkbankSwiftUI
//
//  Created by Onur on 17.12.2022.
//

import Foundation


struct DiscoveryModel : Identifiable {
    var id = UUID()
    var icon : String
    var title : String
}

struct DiscoveryCardsModel : Identifiable {
    var id = UUID()
    let title : String
    let subTitle : String
    let footerTitle : String
    let icon : String
    let reverse : Bool
}

struct DiscoveryContactModel : Identifiable {
    var id = UUID()
    var icon : String
    var title : String
}

class DiscoveryViewModel : ObservableObject {
    
    @Published var discoveryArray : [DiscoveryModel] = []
    
    @Published var shortcuts : [ShortcutsModel] = []
    
    @Published var contacts : [DiscoveryContactModel] = []
    
    
    
    init(){
        getItem()
    }
    
    func getItem() {
        let item1 = DiscoveryModel(icon: "turkishlirasign.circle.fill", title: "Yatirim fonu al/sat")
        
        let item2 = DiscoveryModel(icon: "waveform.path.ecg.rectangle.fill", title: "Yatirimlarim")
        
        let item3 = DiscoveryModel(icon: "bag.fill.badge.plus", title: "Hisse senedi")
        
        let item4 = ShortcutsModel(
            title: "Basvuru ve nakit ihtiyaclar",
            icon: "plus",
            reverse: false
        )
        
        let item5 = ShortcutsModel(
            title: "Kredi islemleri",
            icon: "clock.fill",
            reverse: false
        )
        
        let item6 = ShortcutsModel(
            title: "Tum varliklar",
            icon: "dollarsign.square.fill",
            reverse: false
        )
        
        let item7 = ShortcutsModel(
            title: "Kampanyalar",
            icon: "bookmark.fill",
            reverse: false
        )
        
        let item8 = ShortcutsModel(
            title: "Senin icin",
            icon: "heart.fill",
            reverse: false
        )
        
        let item9 = DiscoveryContactModel(icon: "house", title: "En yakin AKBANK")
        
        let item10 = DiscoveryContactModel(icon: "questionmark.circle", title: "Destek merkezi")
        
        discoveryArray.append(contentsOf: [item1, item2, item3])
        
        shortcuts.append(contentsOf: [item4, item5, item6, item7, item8])
        
        contacts.append(contentsOf: [item9, item10])
    }
}
