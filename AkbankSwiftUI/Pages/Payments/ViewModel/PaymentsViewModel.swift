//
//  PaymentsViewModel.swift
//  AkbankSwiftUI
//
//  Created by Onur on 16.12.2022.
//

import Foundation


struct PaymentsModel : Identifiable {
    var id = UUID()
    var icon : String
    var title : String
}

struct PaymentsCardsModel : Identifiable {
    var id = UUID()
    let title : String
    let subTitle : String
    let footerTitle : String
    let icon : String
    let reverse : Bool
}

class PaymentsViewModel : ObservableObject {
    
    @Published var paymentsArray : [PaymentsModel] = []
    
    @Published var paymentsCards : [PaymentsCardsModel] = []
    
    init(){
        getItem()
    }
    
    func getItem() {
        let item1 = PaymentsModel(icon: "person.badge.key", title: "Para Gonder")
        let item2 = PaymentsModel(icon: "list.and.film", title: "Fatura ve Kurum Odemeleri")
        let item3 = PaymentsModel(icon: "lock.open.iphone", title: "Doviz kurlari")
        
        let item4 = PaymentsCardsModel(
        title: "Gelir bilgini hemen guncelle",
        subTitle: "Sana ozel urun ve firsatlardan yararlanmakicin gelirini hemen guncelle!",
        footerTitle: "Hemen Basvur!",
        icon: "dollarsign.square",
        reverse: true
        )
        
        paymentsArray.append(contentsOf: [item1, item2, item3])
        
        paymentsCards.append(contentsOf: [item4])
    }
}
