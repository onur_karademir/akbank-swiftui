//
//  PaymentsView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 16.12.2022.
//

import SwiftUI

struct PaymentsView: View {
    
    @StateObject var viewmodel : PaymentsViewModel = PaymentsViewModel()
    
@StateObject var homeViewModel : HomeCardsViewModel = HomeCardsViewModel()
    
    var body: some View {
        VStack (alignment: .leading) {
            RegularHeaderView(
                title: "Transfer ve Odemeler",
                titleColor: .white,
                titleFont: .title3,
                titleFontWeight: .bold,
                titleKerning: 1,
                headerBgColor: Color("ColorOne"),
                image: "magnifyingglass.circle.fill",
                imageColor: .white,
                imageFont: .title,
                imageOffset: -10
            )
            VStack (alignment: .leading) {
                
                ScrollView(.vertical, showsIndicators: false) {
                    
                    VStack (alignment: .leading) {
                        
                        HomeDividerTitleView(
                            productTitle: "ONERILEN HIZLI ISLEMLER",
                            productTitleFont: .headline,
                            productTitleFontWeight: .bold,
                            productTitleColor: .gray
                        )
                        
                        HStack (spacing: 10) {
                            // maybe horizontal scrolling can be done for these items, but I didn't see the need because the items were few in the original. //
                            ForEach(viewmodel.paymentsArray) { item in
                                LoginIconRowView(
                                    loginIconCicleColor: .gray.opacity(0.35),
                                    loginIconCicleWidth: 65,
                                    loginIconCicleHeight: 65,
                                    loginIcon: item.icon,
                                    loginIconColor: Color("ColorOne"),
                                    loginIconSize: 25,
                                    loginIconText: item.title,
                                    loginIconTextFont: .caption,
                                    loginIconTextFontWeight: .semibold,
                                    loginIconTextColor: .black.opacity(0.8),
                                    loginIconTextWidth: 89,
                                    loginIconTextHeight: 60
                                )
                            }
                            
                        }
                        
                        HomeDividerTitleView(
                            productTitle: "TRANSFERLER",
                            productTitleFont: .subheadline,
                            productTitleFontWeight: .bold,
                            productTitleColor: .gray
                        )
                        
                        VStack {
                            
                            ForEach(homeViewModel.shortcuts.prefix(3).reversed()) { item in
                                ShortcutsView(
                                    shortcutsCircleColor: .white,
                                    shortcutsIcon: item.icon,
                                    shortcutsIconColor: Color("ColorOne"),
                                    shortcutsTitle: item.title,
                                    shortcutsTitleFont: .headline,
                                    shortcutsTitleFontWeight: .semibold
                                )
                            }
                            
                        }
                        .frame(
                              minWidth: 0,
                              maxWidth: .infinity,
                              minHeight: 0,
                              maxHeight: .infinity,
                              alignment: .topLeading
                            )
                        .padding()
                        .background(Color(.systemGray6))
                        .cornerRadius(10)
                        
                        HomeTitleSetView(
                            homeTitle: "ODEMELER",
                            homeTitleFont: .footnote,
                            homeTitleFontWeight: .bold,
                            homeTitleColor: .gray,
                            homeCardsTitle: "TÜMÜ",
                            homeCardsFont: .caption,
                            homeCardsFontWeight: .bold,
                            homeCardsColor: .red
                        )
                        
                        VStack (spacing: 20) {
                            
                            VStack {
                                
                                ForEach(homeViewModel.shortcuts.shuffled().prefix(3)) { item in
                                    ShortcutsView(
                                        shortcutsCircleColor: .white,
                                        shortcutsIcon: item.icon,
                                        shortcutsIconColor: Color("ColorOne"),
                                        shortcutsTitle: item.title,
                                        shortcutsTitleFont: .headline,
                                        shortcutsTitleFontWeight: .semibold
                                    )
                                }
                                
                            }
                            .frame(
                                minWidth: 0,
                                maxWidth: .infinity,
                                minHeight: 0,
                                maxHeight: .infinity,
                                alignment: .topLeading
                            )
                            .padding()
                            .background(Color(.systemGray6))
                            .cornerRadius(10)
                            
                            ForEach(viewmodel.paymentsCards) { item in
                                HomeCardsView(
                                    homeCardsTitle: item.title,
                                    homeCardsTitleFont: .subheadline,
                                    homeCardsTitleColor: .gray,
                                    homeCardsTitleFontWight: .bold,
                                    homeCardsSubTitle: item.subTitle,
                                    homeCardsSubTitleFont: .callout,
                                    homeCardsSubTitleColor: .black,
                                    homeCardsSubTitleFontWight: .semibold,
                                    homeCardsFooter: item.footerTitle,
                                    homeCardsFooterFont: .subheadline,
                                    homeCardsFooterFontWeight: .semibold,
                                    homeCardsFooterColor: Color("ColorOne"),
                                    homeCardsImage: item.icon,
                                    reverse: item.reverse
                                )
                            }
                            
                            HStack {
                                
                                Spacer()
                                
                                Button {
                                    
                                } label: {
                                    PaymantButtonView(
                                        paymantButtonText: "Transfer ve odemeleri yonet",
                                        paymantButtonFont: .headline,
                                        paymantButtonFontWeight: .bold,
                                        paymantButtonColor: .red,
                                        paymantButtonWidth: 95,
                                        paymantButtonRadius: 25
                                    )
                                }
                                Spacer()
                            }
                          
                            
                        }
                        
                    }
                    
                }
                
            }
            .padding()
        }
        .navigationBarTitle("")
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
}

struct PaymentsView_Previews: PreviewProvider {
    static var previews: some View {
        PaymentsView()
    }
}
