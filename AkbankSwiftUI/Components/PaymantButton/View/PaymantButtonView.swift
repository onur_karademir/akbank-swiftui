//
//  PaymantButtonView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 16.12.2022.
//

import SwiftUI

struct PaymantButtonView: View {
    let paymantButtonText : String
    let paymantButtonFont : Font
    let paymantButtonFontWeight : Font.Weight
    let paymantButtonColor : Color
    let paymantButtonWidth : CGFloat
    let paymantButtonRadius : CGFloat
    var body: some View {
        Text(paymantButtonText)
            .font(paymantButtonFont)
            .fontWeight(paymantButtonFontWeight)
            .foregroundColor(paymantButtonColor)
            .frame(width: UIScreen.main.bounds.width - paymantButtonWidth)
            .padding()
            .overlay(
                RoundedRectangle(cornerRadius: paymantButtonRadius)
                    .stroke(Color.red, lineWidth: 2)
            )
    }
}

struct PaymantButtonView_Previews: PreviewProvider {
    static var previews: some View {
        PaymantButtonView(paymantButtonText: "Transfer ve odemeleri yonet", paymantButtonFont: .headline, paymantButtonFontWeight: .bold, paymantButtonColor: .red, paymantButtonWidth: 95, paymantButtonRadius: 25)
    }
}
