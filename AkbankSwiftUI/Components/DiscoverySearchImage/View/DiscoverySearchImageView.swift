//
//  DiscoverySearchImageView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 18.12.2022.
//

import SwiftUI

struct DiscoverySearchImageView: View {
    let discoverySearchImage : String
    let discoverySearchImageColor : Color
    let discoverySearchImageFont : Font
    var body: some View {
        Image(systemName: discoverySearchImage)
            .foregroundColor(discoverySearchImageColor)
            .font(discoverySearchImageFont)
    }
}

struct DiscoverySearchImageView_Previews: PreviewProvider {
    static var previews: some View {
        DiscoverySearchImageView(discoverySearchImage: "magnifyingglass", discoverySearchImageColor: .white, discoverySearchImageFont: .title2)
    }
}
