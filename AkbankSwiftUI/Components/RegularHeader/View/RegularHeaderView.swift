//
//  RegularHeaderView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 16.12.2022.
//

import SwiftUI

struct RegularHeaderView: View {
    
    let title : String
    let titleColor : Color
    let titleFont : Font
    let titleFontWeight : Font.Weight
    let titleKerning : CGFloat
    
    let headerBgColor : Color
    
    let image : String
    let imageColor : Color
    let imageFont : Font
    let imageOffset : CGFloat
    
    var body: some View {
        HStack {
            Spacer()
            Text(title)
                .foregroundColor(titleColor)
                .font(titleFont)
                .fontWeight(titleFontWeight)
                .kerning(titleKerning)
            Spacer()
        }
        .padding()
        .background(headerBgColor)
        .overlay (
            Image(systemName: image)
                .foregroundColor(imageColor)
                .font(imageFont)
                .offset(x: imageOffset)
            ,alignment: .trailing
        )
    }
}

struct RegularHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        RegularHeaderView(title: "Transfer ve Odemeler", titleColor: .white, titleFont: .title3, titleFontWeight: .bold, titleKerning: 1, headerBgColor: Color("ColorOne"), image: "magnifyingglass.circle.fill", imageColor: .white, imageFont: .title, imageOffset: -10)
    }
}
