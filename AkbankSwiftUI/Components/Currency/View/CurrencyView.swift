//
//  CurrencyView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 15.12.2022.
//

import SwiftUI

struct CurrencyView: View {
    let currency : String
    let currencyFont : Font
    let currencyFontWeight : Font.Weight
    let currencyColor : Color
    var body: some View {
        Text(currency)
            .font(currencyFont)
            .fontWeight(currencyFontWeight)
            .foregroundColor(currencyColor)
    }
}

struct CurrencyView_Previews: PreviewProvider {
    static var previews: some View {
        CurrencyView(currency: "1 USD = 18.63 TL", currencyFont: .caption2, currencyFontWeight: .semibold, currencyColor: .white)
    }
}
