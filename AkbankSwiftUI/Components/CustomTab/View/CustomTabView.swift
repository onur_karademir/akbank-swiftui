//
//  CustomTabView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 15.12.2022.
//

import SwiftUI

struct CustomTabView: View {
    @Binding var selectdTab : String
    var body: some View {
        HStack (spacing: 0) {
            TabBarButtonView(
                image: "house",
                title: "Ana Sayfa",
                iconColor: Color("ColorOne"),
                textColor: Color("ColorOne"),
                selectdTab: $selectdTab
            )
            TabBarButtonView(
                image: "arrow.left.arrow.right.circle",
                title: "Transfer ve Odemeler",
                iconColor: Color("ColorOne"),
                textColor: Color("ColorOne"),
                selectdTab: $selectdTab
            )
            TabBarButtonView(
                image: "safari",
                title: "Kesfet",
                iconColor: Color("ColorOne"),
                textColor: Color("ColorOne"),
                selectdTab: $selectdTab
            )
        }
        .padding()
        .background(Color(.systemGray5))
        .cornerRadius(30)
    }
}

struct CustomTabView_Previews: PreviewProvider {
    static var previews: some View {
        CustomTabView(selectdTab: .constant("house"))
    }
}
