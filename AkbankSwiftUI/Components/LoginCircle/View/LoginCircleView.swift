//
//  LoginCircleView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 18.12.2022.
//

import SwiftUI

struct LoginCircleView: View {
    let loginCircleColor : Color
    let loginCircleWidth : CGFloat
    let loginCircleHeight : CGFloat
    var body: some View {
        Circle()
            .fill(loginCircleColor)
            .frame(width: loginCircleWidth, height: loginCircleHeight, alignment: .center)
    }
}

struct LoginCircleView_Previews: PreviewProvider {
    static var previews: some View {
        LoginCircleView(loginCircleColor: Color("ColorOne"), loginCircleWidth: 35, loginCircleHeight: 35)
    }
}
