//
//  HomeCardsView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 14.12.2022.
//

import SwiftUI

struct HomeCardsView: View {
    let homeCardsTitle : String
    let homeCardsTitleFont : Font
    let homeCardsTitleColor : Color
    let homeCardsTitleFontWight : Font.Weight
    
    let homeCardsSubTitle : String
    let homeCardsSubTitleFont : Font
    let homeCardsSubTitleColor : Color
    let homeCardsSubTitleFontWight : Font.Weight
    
    let homeCardsFooter : String
    let homeCardsFooterFont : Font
    let homeCardsFooterFontWeight : Font.Weight
    let homeCardsFooterColor : Color
    
    let homeCardsImage : String
    
    let reverse : Bool
    
    var body: some View {
        VStack (alignment: .leading, spacing: 25) {
            HStack (alignment: .center, spacing: 10) {
                if reverse {
                    
                    HomeCardsImageView(
                        homeCardsImage: homeCardsImage,
                        firstColor: .red,
                        secondColor: .gray,
                        homeCardsImageFontSize: 64
                    )
                    
                    Spacer()
                    
                    VStack (alignment: .leading, spacing: 15) {
                        Text(homeCardsTitle)
                            .font(homeCardsTitleFont)
                            .foregroundColor(homeCardsTitleColor)
                            .fontWeight(homeCardsTitleFontWight)
                        Text(homeCardsSubTitle)
                            .font(homeCardsSubTitleFont)
                            .foregroundColor(homeCardsSubTitleColor)
                            .fontWeight(homeCardsSubTitleFontWight)
                        Text(homeCardsFooter)
                            .underline()
                            .font(homeCardsFooterFont)
                            .fontWeight(homeCardsFooterFontWeight)
                            .foregroundColor(homeCardsFooterColor)
                    }
                    
                    
                } else {
                    VStack (alignment: .leading, spacing: 15) {
                        Text(homeCardsTitle)
                            .font(homeCardsTitleFont)
                            .foregroundColor(homeCardsTitleColor)
                            .fontWeight(homeCardsTitleFontWight)
                        Text(homeCardsSubTitle)
                            .font(homeCardsSubTitleFont)
                            .foregroundColor(homeCardsSubTitleColor)
                            .fontWeight(homeCardsSubTitleFontWight)
                        Text(homeCardsFooter)
                            .underline()
                            .font(homeCardsFooterFont)
                            .fontWeight(homeCardsFooterFontWeight)
                            .foregroundColor(homeCardsFooterColor)
                    }
                    Spacer()
                    HomeCardsImageView(
                        homeCardsImage: homeCardsImage,
                        firstColor: .red,
                        secondColor: .gray,
                        homeCardsImageFontSize: 64
                    )
                }
                
            }
        }
        .padding()
        .background(Color(.systemGray6))
        .cornerRadius(10)
    }
}

struct HomeCardsView_Previews: PreviewProvider {
    static var previews: some View {
        HomeCardsView(homeCardsTitle: "KREDI KARTI BASVURUSU", homeCardsTitleFont: .subheadline, homeCardsTitleColor: .gray, homeCardsTitleFontWight: .bold, homeCardsSubTitle: "Hemen basvur, toplamda 1000 TL' ye varan chip-para kazan",homeCardsSubTitleFont: .callout, homeCardsSubTitleColor: .black, homeCardsSubTitleFontWight: .semibold, homeCardsFooter: "Hemen Basvur!", homeCardsFooterFont: .subheadline, homeCardsFooterFontWeight: .semibold, homeCardsFooterColor: Color("ColorOne"), homeCardsImage: "rectangle.fill.badge.checkmark", reverse: false)
    }
}
