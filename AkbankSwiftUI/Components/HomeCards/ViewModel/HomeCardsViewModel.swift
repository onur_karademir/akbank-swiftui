//
//  HomeCardsViewModel.swift
//  AkbankSwiftUI
//
//  Created by Onur on 14.12.2022.
//

import Foundation


struct HomeCardsModel : Identifiable {
    var id = UUID()
    let title : String
    let subTitle : String
    let footerTitle : String
    let icon : String
    let reverse : Bool
}

struct ShortcutsModel : Identifiable {
    var id = UUID()
    let title : String
    let icon : String
    let reverse : Bool
}

struct FooterModel : Identifiable {
    var id = UUID()
    let title : String
    let subTitle : String
    let reverse : Bool
}

class HomeCardsViewModel : ObservableObject {
    
    @Published var homeData : [HomeCardsModel] = []
    
    @Published var shortcuts : [ShortcutsModel] = []
    
    @Published var footer : [FooterModel] = []
    
    init () {
        getData()
    }
    
    func getData() {
        
        let item1 = HomeCardsModel(
        title: "KREDI KARTI BASVURUSU",
        subTitle: "Hemen basvur, toplamda 1000 TL' ye varan chip-para kazan",
        footerTitle: "Hemen Basvur!",
        icon: "rectangle.fill.badge.checkmark",
        reverse: false
        )
        
        let item2 = HomeCardsModel(
            title: "ON ONAYLI KREDINIZ HAZIR",
            subTitle: "20.000 TL tutarindaki kredin hemen hesabina gecsin.",
            footerTitle: "Simdi Basvur!",
            icon: "bell.badge.fill",
            reverse: true
        )
        
        let item3 = HomeCardsModel(
            title: "YATIRIMLARIM",
            subTitle: "Yatirim islemi yap ve yatirimlarini takip et.",
            footerTitle: "Yatitimlarimi goruntule",
            icon: "link.badge.plus",
            reverse: false
        )
        
        let item4 = ShortcutsModel(
            title: "Basvuru ve nakit ihtiyaclar",
            icon: "plus",
            reverse: false
        )
        
        let item5 = ShortcutsModel(
            title: "Kredi islemleri",
            icon: "clock.fill",
            reverse: false
        )
        
        let item6 = ShortcutsModel(
            title: "Tum varliklar",
            icon: "dollarsign.square.fill",
            reverse: false
        )
        
        let item7 = ShortcutsModel(
            title: "Kampanyalar",
            icon: "bookmark.fill",
            reverse: false
        )
        
        let item8 = ShortcutsModel(
            title: "Senin icin",
            icon: "heart.fill",
            reverse: false
        )
        
        let item9 = FooterModel(
            title: "SON GIRIS TARIHI",
            subTitle: "Bugun 15:41:01",
            reverse: false
        )
        
        let item10 = FooterModel(
            title: "SON GIRIS YAPILAN KANAL",
            subTitle: "AKBANK Mobil",
            reverse: false
        )
        
        let item11 = FooterModel(
            title: "SON BASARISIZ GIRIS",
            subTitle: "Bugun 15:39:00",
            reverse: false
        )
        
        let item12 = FooterModel(
            title: "SON BASARISIZ GIRIS KANALI",
            subTitle: "AKBANK Mobil",
            reverse: false
        )
        
        homeData.append(contentsOf: [item1, item2, item3])
        
        shortcuts.append(contentsOf: [item4, item5, item6, item7, item8])
        
        footer.append(contentsOf: [item9, item10, item11, item12])
    }
}

