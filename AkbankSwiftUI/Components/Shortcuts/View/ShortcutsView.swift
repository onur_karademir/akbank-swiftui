//
//  ShortcutsView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 14.12.2022.
//

import SwiftUI

struct ShortcutsView: View {
    let shortcutsCircleColor : Color
    let shortcutsIcon : String
    let shortcutsIconColor : Color
    
    let shortcutsTitle : String
    let shortcutsTitleFont : Font
    let shortcutsTitleFontWeight : Font.Weight
    var body: some View {
        HStack (alignment: .center, spacing: 10) {
            Circle()
                .fill(shortcutsCircleColor)
                .frame(width: 55, height: 55, alignment: .center)
                .overlay(
                    Image(systemName: shortcutsIcon)
                        .font(.title)
                        .foregroundColor(shortcutsIconColor)
                )
            Text(shortcutsTitle)
                .font(shortcutsTitleFont)
                .fontWeight(shortcutsTitleFontWeight)
                .kerning(0.7)
            
            Spacer()
        }
        .padding(.horizontal)
        .padding(.vertical, 4)
    }
}

struct ShortcutsView_Previews: PreviewProvider {
    static var previews: some View {
        ShortcutsView(shortcutsCircleColor: Color(.systemGray5), shortcutsIcon: "plus", shortcutsIconColor: Color("ColorOne"), shortcutsTitle: "Basvuru ve nakit ihtiyaclar", shortcutsTitleFont: .title3, shortcutsTitleFontWeight: .semibold)
    }
}
