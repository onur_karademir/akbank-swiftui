//
//  AppVersionView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 15.12.2022.
//

import SwiftUI

struct AppVersionView: View {
    let appVersion : String
    let appVersionFont : Font
    let appVersionFontWeight : Font.Weight
    let appVersionColor : Color
    var body: some View {
        HStack {
            Spacer()
            Text(appVersion)
                .font(appVersionFont)
                .foregroundColor(appVersionColor.opacity(0.6))
                .fontWeight(appVersionFontWeight)
        }
        .padding(.vertical)
    }
}

struct AppVersionView_Previews: PreviewProvider {
    static var previews: some View {
        AppVersionView(appVersion: "V 4.21.1", appVersionFont: .subheadline,appVersionFontWeight: .bold, appVersionColor: .gray)
    }
}
