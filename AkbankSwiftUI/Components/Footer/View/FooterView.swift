//
//  FooterView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 14.12.2022.
//

import SwiftUI

struct FooterView: View {
    let footerTitle : String
    let footerSubTitle : String
    
    let footerTitleFont : Font
    let footerTitleFontWeight : Font.Weight
    let footerTitleColor : Color
    
    let footerSubTitleFont : Font
    let footerSubTitleFontWeight : Font.Weight
    let footerSubTitleColor : Color
    var body: some View {
        HStack {
            Text(footerTitle)
                .font(footerTitleFont)
                .fontWeight(footerTitleFontWeight)
                .foregroundColor(footerTitleColor)
            Spacer()
            Text(footerSubTitle)
                .font(footerSubTitleFont)
                .fontWeight(footerSubTitleFontWeight)
                .foregroundColor(footerSubTitleColor)
        }
        .padding(.horizontal)
        .padding(.vertical, 10)
    }
}

struct FooterView_Previews: PreviewProvider {
    static var previews: some View {
        FooterView(footerTitle: "SON GIRIS TARIHI", footerSubTitle: "SON GIRIS TARIHI", footerTitleFont: .footnote, footerTitleFontWeight: .bold, footerTitleColor: .gray, footerSubTitleFont: .footnote, footerSubTitleFontWeight: .bold, footerSubTitleColor: .gray)
    }
}
