//
//  LoginButtonView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 15.12.2022.
//

import SwiftUI

struct LoginButtonView: View {
    let loginText : String
    let loginButtonWidth : CGFloat
    let loginButtonColor : Color
    let loginButtonPaddingVertical : CGFloat
    let loginButtonBgColor : Color
    var body: some View {
        Text(loginText)
            .frame(width: UIScreen.main.bounds.width - loginButtonWidth)
            .foregroundColor(loginButtonColor)
            .padding(.horizontal)
            .padding(.vertical, loginButtonPaddingVertical)
            .background(loginButtonBgColor)
            .clipShape(Capsule())
    }
}

struct LoginButtonView_Previews: PreviewProvider {
    static var previews: some View {
        LoginButtonView(loginText: "Giris", loginButtonWidth: 150, loginButtonColor: .white, loginButtonPaddingVertical: 15, loginButtonBgColor: Color("ColorOne"))
    }
}
