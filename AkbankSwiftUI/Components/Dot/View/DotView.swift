//
//  DotView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 18.12.2022.
//

import SwiftUI

struct DotView: View {
    let dot : String
    let dotFontWeight : Font.Weight
    var body: some View {
        Text(dot)
            .fontWeight(dotFontWeight)
    }
}

struct DotView_Previews: PreviewProvider {
    static var previews: some View {
        DotView(dot: "***", dotFontWeight: .bold)
    }
}
