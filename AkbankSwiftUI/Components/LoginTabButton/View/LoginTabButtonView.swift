//
//  LoginTabButtonView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 15.12.2022.
//

import SwiftUI

struct LoginTabButtonView: View {
    let loginButtonText : String
    let loginButtonFont : Font
    let loginButtonColor : Color
    let loginButtonFontWeight : Font.Weight
    let loginButtonKerning : CGFloat
    var body: some View {
        Text(loginButtonText)
            .font(loginButtonFont)
            .foregroundColor(loginButtonColor)
            .fontWeight(loginButtonFontWeight)
            .kerning(loginButtonKerning)
    }
}

struct LoginTabButtonView_Previews: PreviewProvider {
    static var previews: some View {
        LoginTabButtonView(loginButtonText: "Kurumsal", loginButtonFont: .title3, loginButtonColor: .black, loginButtonFontWeight: .bold, loginButtonKerning: 1)
    }
}
