//
//  LoginUserIconView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 15.12.2022.
//

import SwiftUI

struct LoginUserIconView: View {
    let loginUserName : String
    let loginUserFont : Font
    let loginUserFontWeight : Font.Weight
    let loginUserNameColor : Color
    
    let loginCircleColor : Color
    let loginCircleWidth : CGFloat
    let loginCircleHeight : CGFloat
    let loginCircleImage : String
    
    let mainCircleColor : Color
    let mainCircleWidth : CGFloat
    let mainCircleHeight : CGFloat
    
    var body: some View {
        VStack {
            LoginMainCircleView(
                mainCircleColor: mainCircleColor,
                mainCircleWidth: mainCircleWidth,
                mainCircleHeight: mainCircleHeight
            )
                .overlay {
                    Text(loginUserName)
                        .font(loginUserFont)
                        .fontWeight(loginUserFontWeight)
                        .foregroundColor(loginUserNameColor)
                }
                .overlay(
                    LoginCircleView(
                        loginCircleColor: loginCircleColor,
                        loginCircleWidth: loginCircleWidth,
                        loginCircleHeight: loginCircleHeight
                    )
                        .overlay(
                            LoginCircleImageView(
                                loginCircleImage: loginCircleImage,
                                loginCircleImageFont: .title3,
                                loginCircleImageColor: .white
                            )
                        )
                        .offset(x: 2, y: 2)
                    ,alignment: .bottomTrailing
                )
        }
    }
}

struct LoginUserIconView_Previews: PreviewProvider {
    static var previews: some View {
        LoginUserIconView(loginUserName: "OK", loginUserFont: .title, loginUserFontWeight: .bold, loginUserNameColor: Color("ColorOne"), loginCircleColor: Color("ColorOne"), loginCircleWidth: 35, loginCircleHeight: 35, loginCircleImage: "person", mainCircleColor: Color(.systemGray5), mainCircleWidth: 90, mainCircleHeight: 90)
    }
}
