//
//  DiscoverySearchView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 18.12.2022.
//

import SwiftUI

struct DiscoverySearchView: View {
    let placeholder : String
    let radius : CGFloat
    // focus state //
    @FocusState private var isFocused: Bool
    @Binding var bindingText : String
    @State var isBig : Bool = false
    
    var body: some View {
        HStack (spacing: 15) {
            HStack (spacing: 8) {
                
                DiscoverySearchImageView(
                    discoverySearchImage: "magnifyingglass",
                    discoverySearchImageColor: .white,
                    discoverySearchImageFont: .title2
                )
                    .scaleEffect(isBig ? 0.6 : 1)
                    .onTapGesture {
                        withAnimation(.easeInOut(duration: 0.4)) {
                            isFocused.toggle()
                            isBig.toggle()
                        }
                    }
                TextField(placeholder, text: $bindingText)
                    .autocapitalization(.none)
                    .tint(.mint)
                    .focused($isFocused)
                    .frame(width: isBig ? UIScreen.main.bounds.width - 120 : 100)
            }
            .padding(.vertical, 10)
            .padding(.horizontal, 15)
            .background(
                RoundedRectangle(cornerRadius: 15)
                    .fill(.black)
                    .opacity(0.2)
            )
        }
        .environment(\.colorScheme, .dark)
    }
}

struct DiscoverySearchView_Previews: PreviewProvider {
    static var previews: some View {
        DiscoverySearchView(placeholder: "Ara", radius: 15, bindingText: .constant(""))
    }
}
