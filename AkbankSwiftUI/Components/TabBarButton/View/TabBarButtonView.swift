//
//  TabBarButtonView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 15.12.2022.
//

import SwiftUI

struct TabBarButtonView: View {
    var image : String
    var title : String
    var iconColor : Color
    var textColor : Color
    @Binding var selectdTab : String
    var body: some View {
        GeometryReader { geo in
            VStack (spacing: 0) {
                Button {
                    withAnimation {
                        selectdTab = image
                    }
                } label: {
                    Image(systemName: "\(image)\(selectdTab == image ? ".fill" : "")")
                        .font(.system(size: 25, weight: .semibold))
                        .foregroundColor(iconColor)
                        .offset(y: selectdTab == image ? -10 : 0)
                    
                }
                .frame(maxWidth: .infinity,  maxHeight: .infinity)
                Text(title)
                    .font(.footnote)
                    .foregroundColor(textColor)
            }

        }
        .frame(height: 50)
    }

}

struct TabBarButtonView_Previews: PreviewProvider {
    static var previews: some View {
        TabBarButtonView(image: "house", title: "Ana Sayfa", iconColor: Color("ColorOne"), textColor: Color("ColorOne"), selectdTab: .constant("house"))
    }
}
