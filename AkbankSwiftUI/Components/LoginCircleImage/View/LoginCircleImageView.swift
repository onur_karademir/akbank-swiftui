//
//  LoginCircleImageView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 18.12.2022.
//

import SwiftUI

struct LoginCircleImageView: View {
    let loginCircleImage : String
    let loginCircleImageFont : Font
    let loginCircleImageColor : Color
    var body: some View {
        Image(systemName: loginCircleImage)
            .font(loginCircleImageFont)
            .foregroundColor(loginCircleImageColor)
    }
}

struct LoginCircleImageView_Previews: PreviewProvider {
    static var previews: some View {
        LoginCircleImageView(loginCircleImage: "person", loginCircleImageFont: .title3, loginCircleImageColor: .white)
    }
}
