//
//  RedLineView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 18.12.2022.
//

import SwiftUI

struct RedLineView: View {
    
    let redLineHeight : CGFloat
    let redLineColor : Color
    let redLineRadius : CGFloat
    
    var body: some View {
        HStack{Spacer()}
            .frame(height: redLineHeight)
            .background(redLineColor)
            .cornerRadius(redLineRadius)
    }
}

struct RedLineView_Previews: PreviewProvider {
    static var previews: some View {
        RedLineView(redLineHeight: 10, redLineColor: Color("ColorOne"), redLineRadius: 20)
    }
}
