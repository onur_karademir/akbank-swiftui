//
//  LoginMainCircleView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 18.12.2022.
//

import SwiftUI

struct LoginMainCircleView: View {
    
    let mainCircleColor : Color
    let mainCircleWidth : CGFloat
    let mainCircleHeight : CGFloat
    
    var body: some View {
        Circle()
            .fill(mainCircleColor)
            .frame(width: mainCircleWidth, height: mainCircleHeight, alignment: .center)
    }
}

struct LoginMainCircleView_Previews: PreviewProvider {
    static var previews: some View {
        LoginMainCircleView(mainCircleColor: Color(.systemGray5), mainCircleWidth: 90, mainCircleHeight: 90)
    }
}
