//
//  LaunchLogoView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 14.12.2022.
//

import SwiftUI

struct LaunchLogoView: View {
    let launchLogoTitle : String
    let launchLogoSize : CGFloat
    let launchLogoWeight : Font.Weight
    let launchLogoColor : Color
    var body: some View {
        Text(launchLogoTitle)
            .font(.system(size: launchLogoSize, weight: launchLogoWeight, design: .rounded))
            .foregroundColor(launchLogoColor)
            .kerning(1.2)
            .padding()
    }
}

struct LaunchLogoView_Previews: PreviewProvider {
    static var previews: some View {
        LaunchLogoView(launchLogoTitle: "AKBANK", launchLogoSize: 60, launchLogoWeight: .bold, launchLogoColor: .white)
    }
}
