//
//  LoginTabView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 15.12.2022.
//

import SwiftUI

struct LoginTabView: View {
    @State var activeTab : Int = 0
    var body: some View {
        HStack {
            Button {
                withAnimation {
                    activeTab = 0
                }
            } label: {
                VStack {
                    LoginTabButtonView(
                        loginButtonText: "Bireysel",
                        loginButtonFont: .title3,
                        loginButtonColor: .black,
                        loginButtonFontWeight: .bold,
                        loginButtonKerning: 1
                    )
                    HStack{Spacer()}
                        .frame(height: 3)
                        .background(activeTab == 0 ? Color("ColorOne") : .clear)
                }
                .frame(maxWidth: 150)
            }
            
            Spacer()
            
            Button {
                withAnimation {
                    activeTab = 1
                }
            } label: {
                VStack {
                    LoginTabButtonView(
                        loginButtonText: "Kurumsal",
                        loginButtonFont: .title3,
                        loginButtonColor: .black,
                        loginButtonFontWeight: .bold,
                        loginButtonKerning: 1
                    )
                    
                    HStack{Spacer()}
                        .frame(height: 3)
                        .background(activeTab == 1 ? Color("ColorOne") : .clear)
                }
                .frame(maxWidth: 150)
            }

        }
        .padding()
    }
}

struct LoginTabView_Previews: PreviewProvider {
    static var previews: some View {
        LoginTabView()
    }
}
