//
//  LoginTitleSetView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 15.12.2022.
//

import SwiftUI

struct LoginTitleSetView: View {
    let title : String
    let subTitle : String
    let fontSize : CGFloat
    
    let question : String
    let questionColor : Color
    
    var body: some View {
        VStack (spacing: 20) {
            
            VStack {
                Text(title)
                    
                Text(subTitle)
            }
            .font(.system(size: fontSize, weight: .bold, design: .rounded))
            
            Text(question)
                .font(.headline)
                .foregroundColor(questionColor)
        }
    }
}

struct LoginTitleSetView_Previews: PreviewProvider {
    static var previews: some View {
        LoginTitleSetView(title: "İyi Günler", subTitle: "ONUR", fontSize: 45, question: "Yoksa Onur degil misin?", questionColor: Color("ColorOne"))
    }
}
