//
//  HomeDividerTitleView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 14.12.2022.
//

import SwiftUI

struct HomeDividerTitleView: View {
    
    let productTitle : String
    let productTitleFont : Font
    let productTitleFontWeight : Font.Weight
    let productTitleColor : Color
    
    var body: some View {
        HStack {
            Text(productTitle)
                .font(productTitleFont)
                .fontWeight(productTitleFontWeight)
                .foregroundColor(productTitleColor)
            Spacer()
        }
    }
}

struct HomeDividerTitleView_Previews: PreviewProvider {
    static var previews: some View {
        HomeDividerTitleView(productTitle: "URUNLER", productTitleFont: .headline, productTitleFontWeight: .bold, productTitleColor: .gray)
    }
}
