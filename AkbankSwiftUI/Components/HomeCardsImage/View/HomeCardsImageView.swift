//
//  HomeCardsImageView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 18.12.2022.
//

import SwiftUI

struct HomeCardsImageView: View {
    
    let homeCardsImage : String
    let firstColor : Color
    let secondColor : Color
    let homeCardsImageFontSize : CGFloat
    
    var body: some View {
        Image(systemName: homeCardsImage)
            .symbolRenderingMode(.palette)
            .foregroundStyle(firstColor, secondColor)
            .font(.system(size: homeCardsImageFontSize))
    }
}

struct HomeCardsImageView_Previews: PreviewProvider {
    static var previews: some View {
        HomeCardsImageView(homeCardsImage: "rectangle.fill.badge.checkmark", firstColor: .red, secondColor: .gray, homeCardsImageFontSize: 64)
    }
}
