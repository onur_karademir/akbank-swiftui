//
//  LoginIconRowView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 15.12.2022.
//

import SwiftUI

struct LoginIconRowView: View {
    let loginIconCicleColor : Color
    let loginIconCicleWidth : CGFloat
    let loginIconCicleHeight : CGFloat
    
    let loginIcon : String
    let loginIconColor : Color
    let loginIconSize : CGFloat
    
    let loginIconText : String
    let loginIconTextFont : Font
    let loginIconTextFontWeight : Font.Weight
    let loginIconTextColor : Color
    let loginIconTextWidth : CGFloat
    let loginIconTextHeight : CGFloat
    
    var body: some View {
        VStack (alignment: .center, spacing: 2) {
            Circle()
                .fill(loginIconCicleColor)
                .frame(width: loginIconCicleWidth, height: loginIconCicleHeight, alignment: .center)
                .overlay(
                    Image(systemName: loginIcon)
                        .foregroundColor(loginIconColor)
                        .font(.system(size: loginIconSize))
                )
            Text(loginIconText)
                .font(loginIconTextFont)
                .fontWeight(loginIconTextFontWeight)
                .foregroundColor(loginIconTextColor)
                .frame(width: loginIconTextWidth, height: loginIconTextHeight)
                .multilineTextAlignment(.center)
        }
    }
}

struct LoginIconRowView_Previews: PreviewProvider {
    static var previews: some View {
        LoginIconRowView(loginIconCicleColor: .white, loginIconCicleWidth: 60, loginIconCicleHeight: 60, loginIcon: "dots.and.line.vertical.and.cursorarrow.rectangle", loginIconColor: Color("ColorOne"), loginIconSize: 30, loginIconText: "Piyasa Bilgileri", loginIconTextFont: .caption, loginIconTextFontWeight: .semibold, loginIconTextColor: .white, loginIconTextWidth: 100, loginIconTextHeight: 60)
    }
}
