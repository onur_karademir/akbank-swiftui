//
//  HomeHeaderView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 14.12.2022.
//

import SwiftUI

struct HomeHeaderView: View {
    let title : String
    let titleFont : Font
    let titleFontWeight : Font.Weight
    let titleColor : Color
    
    let userName : String
    let userNameColor : Color
    let userNameFont : Font
    var body: some View {
        HStack {
            Text(title)
                .font(titleFont)
                .fontWeight(titleFontWeight)
                .kerning(1)
                .foregroundColor(titleColor)
            Spacer()
            LoginMainCircleView(
                mainCircleColor: Color(.systemGray5),
                mainCircleWidth: 40,
                mainCircleHeight: 40
            )
                .overlay(
                    Text(userName)
                        .font(userNameFont)
                        .foregroundColor(userNameColor)
                )
        }
        .padding(10)
        .background(Color("ColorOne"))
    }
}

struct HomeHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        HomeHeaderView(title: "AKBANK", titleFont: .title2, titleFontWeight: .bold, titleColor: .white, userName: "OK", userNameColor: .red, userNameFont: .headline)
    }
}
