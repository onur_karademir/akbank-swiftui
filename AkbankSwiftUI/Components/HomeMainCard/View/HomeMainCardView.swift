//
//  HomeMainCardView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 14.12.2022.
//

import SwiftUI

struct HomeMainCardView: View {
    let mainCardTitle : String
    let mainCardTitleFont : Font
    let mainCardTitleFontWeight : Font.Weight
    
    let mainCardNum : String
    let mainCardNumFont : Font
    let mainCardNumFontWeight : Font.Weight
    let mainCardNumColor : Color
    
    let mainCardSubTitle : String
    let mainCardSubTitleFont : Font
    let mainCardSubTitleFontWeight : Font.Weight
    let mainCardSubTitleColor : Color
    
    let mainCardMoney : String
    let mainCardMoneyFont : Font
    let mainCardMoneyFontWeight : Font.Weight
    
    let accountType : String
    let accountTypeFont : Font
    let accountTypeFontWeight : Font.Weight
    let accountTypeColor : Color
    var body: some View {
        VStack (alignment: .leading, spacing: 25) {
            
            HStack (alignment: .top) {
                
                VStack (alignment: .leading, spacing: 16) {
                    Text(mainCardTitle)
                        .font(mainCardTitleFont)
                        .fontWeight(mainCardTitleFontWeight)
                    Text(mainCardNum)
                        .font(mainCardNumFont)
                        .fontWeight(mainCardNumFontWeight)
                        .foregroundColor(mainCardNumColor)
                }
                
                Spacer()
                
                DotView(
                    dot: "***",
                    dotFontWeight: .bold
                )
            }
            Text(mainCardSubTitle)
                .font(mainCardSubTitleFont)
                .fontWeight(mainCardSubTitleFontWeight)
                .foregroundColor(mainCardSubTitleColor)
            
            HStack {
                Text(mainCardMoney)
                    .font(mainCardMoneyFont)
                    .fontWeight(mainCardMoneyFontWeight)
                Spacer()
                Text(accountType)
                    .font(accountTypeFont)
                    .fontWeight(accountTypeFontWeight)
                    .foregroundColor(accountTypeColor)
            }
            
        }
        .padding()
        .background(Color(.systemGray6))
        .cornerRadius(10)
        .overlay(
            RedLineView(
                redLineHeight: 10,
                redLineColor: Color("ColorOne"),
                redLineRadius: 20
            )
            ,alignment: .bottom
        )
    }
}

struct HomeMainCardView_Previews: PreviewProvider {
    static var previews: some View {
        HomeMainCardView(mainCardTitle: "ORNEK MAHALLESI", mainCardTitleFont: .subheadline, mainCardTitleFontWeight: .bold, mainCardNum: "1234 - 005678", mainCardNumFont: .footnote, mainCardNumFontWeight: .bold, mainCardNumColor: .gray, mainCardSubTitle: "Kullanilabilir bakiye", mainCardSubTitleFont: .headline, mainCardSubTitleFontWeight: .bold, mainCardSubTitleColor: .gray, mainCardMoney: "10.000,50 TL", mainCardMoneyFont: .title, mainCardMoneyFontWeight: .bold, accountType: "VADESIZ", accountTypeFont: .subheadline, accountTypeFontWeight: .semibold, accountTypeColor: .black)
    }
}
