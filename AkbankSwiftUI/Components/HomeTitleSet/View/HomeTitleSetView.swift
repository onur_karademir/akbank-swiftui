//
//  HomeTitleSetView.swift
//  AkbankSwiftUI
//
//  Created by Onur on 14.12.2022.
//

import SwiftUI

struct HomeTitleSetView: View {
    let homeTitle : String
    let homeTitleFont : Font
    let homeTitleFontWeight : Font.Weight
    let homeTitleColor : Color
    
    let homeCardsTitle : String
    let homeCardsFont : Font
    let homeCardsFontWeight : Font.Weight
    let homeCardsColor : Color
    var body: some View {
        HStack {
            Text(homeTitle)
                .font(homeTitleFont)
                .foregroundColor(homeTitleColor)
                .fontWeight(homeTitleFontWeight)
                .kerning(1)
            Spacer()
            Text(homeCardsTitle)
                .font(homeCardsFont)
                .fontWeight(homeCardsFontWeight)
                .foregroundColor(homeCardsColor)
                .underline()
        }
        .padding()
    }
}

struct HomeTitleSetView_Previews: PreviewProvider {
    static var previews: some View {
        HomeTitleSetView(homeTitle: "HESAPLAR VE KARTLAR", homeTitleFont: .headline, homeTitleFontWeight: .bold, homeTitleColor: .gray, homeCardsTitle: "TÜMÜ", homeCardsFont: .caption, homeCardsFontWeight: .bold, homeCardsColor: .red)
    }
}
